#! /usr/bin/env python
#
# run as:
# sudo -u wcs wcsctl runscript index-formdefs-in-elasticsearch.py --vhost=...

from elasticsearch import Elasticsearch

import wcs
from wcs.formdef import FormDef

es = Elasticsearch([{'host': '46.18.194.18', 'port': 9200}])

for formdef in FormDef.select():
    if formdef.disabled and not formdef.disabled_redirection:
        continue
    url = formdef.get_url() if not formdef.disabled else formdef.disabled_redirection
    es.index(index='liferay-20116',
             doc_type='LiferayDocumentType',
             id="wcs:formdef:%s" % formdef.id,
             body={
                 'title': formdef.name,
                 'url': formdef.get_url(),
                 'description': '',
                 'type': 'procedure'
            })
