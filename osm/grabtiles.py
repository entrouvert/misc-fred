import math
import os
import requests
import sys

def deg2num(lat_deg, lon_deg, zoom):
    lat_rad = math.radians(lat_deg)
    n = 2.0 ** zoom
    xtile = int((lon_deg + 180.0) / 360.0 * n)
    ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
    return (xtile, ytile)


cities = [
    # Grand Lyon
    {
        "corner1": {"lat": 45.9134, "lng": 4.6733},
        "corner2": {"lat": 45.5583, "lng": 5.1574}
    },
    # Clermont-Ferrand
    {
        "corner1": {"lat": 45.8077, "lng": 3.0375},
        "corner2": {"lat": 45.7187, "lng": 3.2053}
    },
    # Grenoble
    {
        "corner1": {"lat": 45.2307, "lng": 5.6640},
        "corner2": {"lat": 45.1179, "lng": 5.7980}
    },
    # Quimper
    {
        "corner1": {"lat": 48.0657, "lng": -4.2218},
        "corner2": {"lat": 47.9380, "lng": -3.9647}
    },
]

def update_progress(label, progress, num_columns=120):
    sys.stdout.write('[%s] [%s] %s%%\r' % (
        label, ('#'*((num_columns-10)*progress/100)).ljust(num_columns-10), progress))

for map_max_bounds in cities:
    print ''
    for zoom in range(14, 20):
        xtile1, ytile1 = deg2num(map_max_bounds['corner1']['lat'],
                                 map_max_bounds['corner1']['lng'],
                                 zoom)
        xtile2, ytile2 = deg2num(map_max_bounds['corner2']['lat'],
                                 map_max_bounds['corner2']['lng'],
                                 zoom)
        num_tiles = (1 + xtile2 - xtile1) * (1 + ytile2 - ytile1)
        idx = 0
        for xtile in range(xtile1, xtile2+1):
            for ytile in range(ytile1, ytile2+1):
                update_progress(zoom, int(math.ceil(100.0 * idx / num_tiles)))
                idx += 1
                path = '%s/%s/%s.png' % (zoom, xtile, ytile)
                if os.path.exists(path):
                    continue
                url = 'https://tiles.entrouvert.org/hdm/%s/%s/%s.png' % (zoom, xtile, ytile)
                if False:
                    # store
                    if not os.path.exists(os.path.dirname(path)):
                        os.makedirs(os.path.dirname(path))
                    r = requests.get(url)
                    if r.status_code != 200:
                        continue
                    with open(path, 'w') as fd:
                        fd.write(r.content)
                else:
                    # just make sure tile is in cache
                    requests.head(url)
        print ''
