import json
import sys

from wcs.formdef import FormDef

if len(sys.argv) == 2:
    data = json.load(open(sys.argv[1]))
else:
    data = json.load(sys.stdin)

for key in data:
    try:
        formdef = FormDef.get_by_urlname(key)
    except KeyError:
        continue
    plus1_field_id = None
    for field in formdef.workflow.backoffice_fields_formdef.fields:
        if field.varname == 'plus1':
            plus1_field_id = field.id
            break
    if not plus1_field_id:
        continue
    dataclass = formdef.data_class()
    for data_id, data_count in data[key].items():
        formdata = dataclass.get(data_id)
        if str(data_count) != formdata.data.get(plus1_field_id):
            formdata.data[plus1_field_id] = str(data_count)
        formdata.store()
