#! /usr/bin/env python

# THIS IS OBSOLETE
# actual script is a management command in authentic2-gnm

import requests
import sys

from django.conf import settings
from django.contrib.auth import get_user_model

from authentic2.a2_rbac.models import OrganizationalUnit
from authentic2_auth_oidc.models import OIDCProvider, OIDCAccount

ou_mapping = {
    'gl-guichet-numerique': OrganizationalUnit.objects.get(slug='grand-lyon'),
    'bron': OrganizationalUnit.objects.get(slug='hobo-bron'),
    'dardilly': OrganizationalUnit.objects.get(slug='hobo-dardilly'),
    'oullins': OrganizationalUnit.objects.get(slug='hobo-oullins'),
    'vaulx-en-velin': OrganizationalUnit.objects.get(slug='hobo-vaulx-en-velin'),
    #'villeurbanne': OrganizationalUnit.objects.get(slug='hobo-villeurbanne'),

    # new names
    'gnm': OrganizationalUnit.objects.get(slug='grand-lyon'),
    'gnm-bron': OrganizationalUnit.objects.get(slug='hobo-bron'),
    'gnm-dardilly': OrganizationalUnit.objects.get(slug='hobo-dardilly'),
    'gnm-oullins': OrganizationalUnit.objects.get(slug='hobo-oullins'),
    'gnm-vaulx-en-velin': OrganizationalUnit.objects.get(slug='hobo-vaulx-en-velin'),

    # new new names
    'gl-guichet-numerique-bron': OrganizationalUnit.objects.get(slug='hobo-bron'),
    'gl-guichet-numerique-dardilly': OrganizationalUnit.objects.get(slug='hobo-dardilly'),
    'gl-guichet-numerique-oullins': OrganizationalUnit.objects.get(slug='hobo-oullins'),
    'gl-guichet-numerique-vaulx-en-velin': OrganizationalUnit.objects.get(slug='hobo-vaulx-en-velin'),

    'territoire': OrganizationalUnit.objects.get(slug='grand-lyon'),
}

cut_agents = OIDCProvider.objects.get(name='cut-agents')

User = get_user_model()

for ou_slug, ou in ou_mapping.items():
    url = 'https://admin-cut-rec.grandlyon.com/api/users/?ou__slug=%s' % ou_slug
    for cut_user_data in requests.get(url, auth=settings.CUT_API_CREDENTIALS).json()['results']:
        try:
            user = User.objects.get(oidc_account__provider=cut_agents,
                                    oidc_account__sub=cut_user_data['sub'])
        except User.DoesNotExist:
            pass
        try:
            user = User.objects.get(email=cut_user_data['email'])
        except User.DoesNotExist:
            user = User(first_name=cut_user_data['first_name'],
                    last_name=cut_user_data['last_name'],
                    email=cut_user_data['email'],
                    ou=ou)
        user.uuid = cut_user_data['sub']
        user.ou = ou
        user.first_name = cut_user_data['first_name']
        user.last_name = cut_user_data['last_name']
        user.save()
        OIDCAccount.objects.get_or_create(provider=cut_agents, user=user, sub=cut_user_data['sub'])
