from wcs.formdef import FormDef
from wcs import sql

slugs = [
        'ambroisie',
        'arbres-et-espaces-verts',
        'collecte-et-proprete',
        'dechets-et-proprete',
        'deneigement',
        'eau-et-assainissement',
        'eclairage',
        'nuisance-olfactives-animales-vegetales',
        'objets-perdus',
        'voirie-et-signalisation',
        ]

#modele = FormDef.get_by_urlname('modele-signalement')
#modele_fields = {x.varname: x.id for x in modele.fields if x.varname}
modele_fields = {'Information_usager': '133', 'numero': '104', 'prenom_agent': '142', 'nom_usager': '135', 'voie': '105', 'tel_usager': '138', 'nom': '121', 'photo': '116', 'signalement_proximite': '108', 'type_probleme': '113', 'courriel_usager': '137', 'piecejointe_backoffice': '130', 'consentement_formulaire': '127', 'carte': '103', 'prenom_usager': '136', 'destinataire_backoffice': '131', 'prenom': '122', 'support_entree': '129', 'telephone': '124', 'nom_agent': '143', 'commentaire_backoffice': '140', 'commune': '106', 'message': '115', 'mode_reponse': '139', 'photo2': '117', 'date_demande': '132', 'courriel': '123', 'civilite': '120'}


conn, cur = sql.get_connection_and_cursor()

for slug in slugs:
    formdef = FormDef.get_by_urlname(slug)
    mapping = {x.id: modele_fields[x.varname] for x in formdef.fields if x.varname}
    existing_ids = [x.id for x in formdef.fields]
    changes = {x: y for x, y in mapping.items() if x != y}
    if not changes:
        continue
    table_name = sql.get_formdef_table_name(formdef)
    cur.execute('''SELECT column_name FROM information_schema.columns
                    WHERE table_schema = 'public'
                      AND table_name = %s''', (table_name,))
    existing_fields = set([x[0] for x in cur.fetchall()])

    for orig_id, target_id in mapping.items():
        if target_id in existing_ids:
            print(target_id, 'exists')
        else:
            field = [x for x in formdef.fields if x.id == orig_id][0]
            field.id = target_id
            cur.execute('''ALTER TABLE %s RENAME COLUMN f%s TO f%s''' % (
                table_name, orig_id, target_id))
            if 'f%s_display' % orig_id in existing_fields:
                cur.execute('''ALTER TABLE %s RENAME COLUMN f%s_display TO f%s_display''' % (
                    table_name, orig_id, target_id))
            if 'f%s_structured' % orig_id in existing_fields:
                cur.execute('''ALTER TABLE %s RENAME COLUMN f%s_structured TO f%s_structured''' % (
                    table_name, orig_id, target_id))
    sql.drop_views(None, conn, cur)
    conn.commit()
    formdef.store()

sql.migrate_views(conn, cur)
conn.commit()
cur.close()
