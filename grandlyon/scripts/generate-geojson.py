import json
import os

from quixote import get_publisher

from qommon.misc import JSONEncoder
from qommon.storage import Contains

from django.utils.encoding import force_text

from wcs.categories import Category
from wcs.formdef import FormDef
from wcs.backoffice.management import geojson_formdatas

geojson_dir = os.path.join(get_publisher().app_dir, 'geojsons')
if not os.path.exists(geojson_dir):
    os.mkdir(geojson_dir)

category = [x for x in Category.select() if x.name == 'Signalements'][0]

for formdef in FormDef.select():
    if not formdef.category_id == category.id:
        continue
    applied_filters = ['wf-%s' % x.id for x in formdef.workflow.get_not_endpoint_status()]

    formdatas = formdef.data_class().select([Contains('status', applied_filters)])

    geojson = geojson_formdatas(formdatas)
    for feature in geojson['features']:
        formdata_id = feature['properties']['url'].split('/')[-2]
        formdata = formdef.data_class().get(formdata_id)
        formdata_vars = formdata.get_substitution_variables()
        for field in ('numero', 'voie', 'commune', 'message', 'type_probleme'):
            feature['properties'][field] = force_text(formdata_vars.get('form_var_%s' % field))
        feature['properties']['datetime'] = formdata_vars['form_receipt_datetime'].strftime('%d/%m/%Y %H:%M')
        feature['properties']['reference'] = '%s:%s' % (formdata_vars['form_slug'], formdata_id)

    json.dump(geojson, open(os.path.join(geojson_dir, formdef.url_name + '.json'), 'w'),
            indent=2, cls=JSONEncoder)
