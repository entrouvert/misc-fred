# iterate over all objects and store them again to make sure side-code is run.

from quixote import get_publisher
from wcs.formdef import FormDef

for formdef in FormDef.select():
    formdef.store()

