#! /usr/bin/env python

import json
import os
import subprocess
import sys

from django.utils.text import slugify

communes_json = os.path.join(os.path.dirname(__file__), 'communes.json')

orig_city = json.load(open(communes_json))['orig']
slug_orig_city = slugify(orig_city)
orig_site = 'agendas-%s.guichet-recette.grandlyon.com' % slug_orig_city

site_export_orig = subprocess.check_output(['sudo', '-u', 'chrono', 'chrono-manage', 'tenant_command', 'export_site', '-d', orig_site])

for dest in json.load(open(communes_json))['dests']:
    dest_city = dest['name']
    slug_dest_city = slugify(dest_city)
    dest_site = 'agendas-%s.guichet-recette.grandlyon.com' % slug_dest_city

    site_export = site_export_orig.replace(orig_city, dest_city)
    site_export = site_export.replace(slug_orig_city, slug_dest_city)
    open('/tmp/site-export.json', 'w').write(site_export)
    subprocess.call(['sudo', '-u', 'chrono', 'chrono-manage', 'tenant_command', 'import_site', '-d', dest_site, '/tmp/site-export.json'])
