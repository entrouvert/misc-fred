#! /usr/bin/env python

import json

from passerelle.apps.jsondatastore.models import JsonDataStore, JsonData

plus1_store = JsonDataStore.objects.get(slug='plus1')

formdefs = {}

for data in JsonData.objects.filter(datastore=plus1_store):
    if not 'reference' in data.content:
        continue
    if not ':' in data.content['reference']:
        continue
    formdef_slug, formdata_id = data.content['reference'].split(':')
    if not formdef_slug in formdefs:
        formdefs[formdef_slug] = {}
    if not formdata_id in formdefs[formdef_slug]:
        formdefs[formdef_slug][formdata_id] = 0
    formdefs[formdef_slug][formdata_id] = formdefs[formdef_slug][formdata_id] + 1

print json.dumps(formdefs)
