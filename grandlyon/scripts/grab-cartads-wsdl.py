#! /usr/bin/env python

from StringIO import StringIO
from lxml import etree as ET
import requests
import time

urls = []
for wsdl in ('ServiceDocumentation.svc', 'ServiceEtapeDossier.svc', 'ServicePortail.svc', 'ServiceSuiviNumerique.svc', 'ServiceDossier.svc', 'ServicePiece.svc', 'ServiceRechercheDossier.svc'):
    urls.append((wsdl, 'https://api-rec.grandlyon.com/ads-rec/adscs/webservices/%s?wsdl' % wsdl))

def indent_xml(elem, level=0):
    # in-place prettyprint formatter
    # http://effbot.org/zone/element-lib.htm#prettyprint
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        for elem in elem:
            indent_xml(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
    return elem

import_counter = 1

i = 0
while i < len(urls):
    time.sleep(0.1)
