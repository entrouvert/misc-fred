#! /usr/bin/env python

import cgi
import os
import subprocess
import sys

print 'Content-type: image/png\n'

cmd = ['convert', 'codereview-base.png']
qs = os.environ.get('QUERY_STRING', '')
for c in '1234567':
    if c in qs:
        cmd.extend(['codereview-%s.png' % c, '-composite'])

cmd.append('png:-')
sys.stdout.write(subprocess.check_output(cmd))
