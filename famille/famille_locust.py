# locust -f test_locust.py
# ou juste python test_locust.py pour synchrone / utile pdb

import logging
import random
import re
import time
from locust import HttpUser, task, between, run_single_user
import urllib.parse
from requests.models import Response

import locust.clients

DELAYS = True


with open('comptes_tests_de_charge.txt') as fd:
    accounts = [x.strip() for x in fd.readlines() if x.strip()]


def get_name(url):
    parsed = urllib.parse.urlparse(url)
    return f'{parsed.netloc.replace("-publik-famille.test.entrouvert.org", "")} - {re.sub("/[0-9]+/?", "/…", parsed.path)}'


class PublikResponse(Response):

    @property
    def pq(self):
        from pyquery import PyQuery

        return PyQuery(self.text)

    def click(self, selector, href=None, **kwargs):
        from pyquery import PyQuery

        pq = self.pq
        matches = None
        try:
            matches = pq.find('selector')
        except SyntaxError:
            pass
        else:
            if matches.count == 0:
                matches = None
        if matches:
            href = matches.attr.href
        else:
            links = [
                x
                for x in pq.find('a')
                if PyQuery(x).text().lower() == selector.lower()
                and (href is None or href in x.attrib.get('href', ''))
            ]
            if not links:
                # partial match
                links = [
                    x
                    for x in pq.find('a')
                    if selector.lower() in PyQuery(x).text().lower()
                    and (href is None or href in x.attrib.get('href', ''))
                ]
            href = links[0].attrib['href']
        href = urllib.parse.urljoin(self.request.url, href)
        return self.client.get(href, name=get_name(href), **kwargs)

    def form(self):
        return self.pq('form')

    def get_wcs_page_name(self):
        return '%s' % self.pq('head title').text().split('|')[0]

    def do_wcs_live_request(self):
        form = self.form()
        form_data = form.serialize_dict()
        form_data['modified_field_id[]'] = 'init'
        for prefilled in self.pq('.widget-prefilled'):
            form_data['prefilled_%s' % prefilled.attrib['data-field-id']] = 'true'
        return self.client.post(form.attr['data-live-url'], form_data,
                                name='[live] %s' % self.get_wcs_page_name())

    def do_wcs_next_page(self, params=None):
        form = self.form()
        form_data = form.serialize_dict()
        form_data.update(params or {})
        form_data['submit'] = 'Suivant'
        return self.client.post(urllib.parse.urljoin(self.request.url, form.attr['action']), form_data,
                                name=self.get_wcs_page_name())


class HttpAdapter(locust.clients.LocustHttpAdapter):
    def __init__(self, client, **kwargs):
        super().__init__(**kwargs)
        self.client = client

    def build_response(self, req, resp):
        from pyquery import PyQuery

        response = super().build_response(req, resp)
        response.__class__ = PublikResponse
        response.client = self.client
        try:
            pq = PyQuery(response.text)
        except Exception:
            pass
        else:
            # load ajax cells
            for cell_div in pq.find('.cell[data-ajax-cell-url]'):
                if not PyQuery(cell_div).find('[data-ajax-cell-must-load]'):
                    continue
                ajax_url = url = cell_div.attrib['data-ajax-cell-url']
                if not urllib.parse.urlparse(url).scheme:
                    url = urllib.parse.urljoin(response.request.url, url)

                url += '?ctx=%s' % cell_div.attrib['data-extra-context']
                # print('loading via ajax')
                cell_response = self.client.get(url, name=get_name(url))
                cell_response.encoding = 'utf-8'

                def repl(match):
                    return re.sub(
                        '><div>.*', f'><div>{cell_response.text}</div></div>', match.group(), flags=re.DOTALL
                    )

                response._content = re.sub(
                    f'<div[^<]*?data-ajax-cell-url="{ajax_url}".*?><div>(.*?)</div></div>',
                    repl,
                    response.text,
                    flags=re.DOTALL,
                ).encode()
                pass
        return response


class PublikUser(HttpUser):
    abstract = True

    def __init__(self, env, **kwargs):
        super().__init__(env, **kwargs)
        # print('env:', env.__dict__)
        self.client.adapters['https://'] = HttpAdapter(
            client=self.client, pool_manager=self.client.adapters['https://'].poolmanager
        )
        self.client.adapters['http://'] = HttpAdapter(
            client=self.client, pool_manager=self.client.adapters['http://'].poolmanager
        )


class PublikFamilleNantesUser(PublikUser):
    host = 'https://portail-publik-famille.test.entrouvert.org'

    total_sleep = 0

    def reset_timer(self):
        self.total_sleep = 0
        self.t0 = time.time()

    def sleep(self, duration):
        if not DELAYS:
            return
        if not self.total_sleep:
            self.total_sleep = 0
        real_sleep = duration * random.random() * 5
        self.total_sleep += real_sleep
        time.sleep(real_sleep)

    def get_elapsed_time(self):
        total_time = time.time() - self.t0
        real_time = total_time - self.total_sleep
        return {'total': total_time, 'real': real_time}

    @task
    def hello_world(self):
        self.reset_timer()
        self.client.cookies.clear()
        response = self.client.get(
            'https://portail-publik-famille.test.entrouvert.org/',
            name=get_name('https://portail-publik-famille.test.entrouvert.org/'),
        )
        response = response.click('Se connecter au portail')
        from pyquery import PyQuery

        login_form = PyQuery(response.text).find('#login-password-form')
        login_data = login_form.serialize_dict()
        login_data['username'] = random.choice(accounts)
        logging.info('account: %s' % login_data['username'])
        login_data['password'] = 'test_de_charge'
        login_data['login-password-submit'] = ''
        self.sleep(3)
        response = self.client.post(
            response.request.url,
            login_data,
            headers={'REFERER': response.request.url},
            name=get_name(response.request.url),
        )
        assert response.request.url == 'https://portail-publik-famille.test.entrouvert.org/portail-famille/'

        response = response.click('Famille', href='page-famille')
        child = random.choice(PyQuery(response.content)('.wcs_wcscardcell-180 td:first-child a'))
        # child_url = child.attrib['href']
        child_name = PyQuery(child).text()
        logging.info('enfant: %s' % child_name)
        response = response.click(child_name)

        response = response.click('Création/Mise à jour DUE')
        self.sleep(5)
        assert response.request.url.endswith('page-enfant-inscription/')

        response = response.click('Alsh vacances')
        self.sleep(5)
        response = response.do_wcs_next_page()

        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Régime alimentaire'
        self.sleep(5)
        response = response.do_wcs_next_page()

        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Inscription'
        response.do_wcs_live_request()
        if not response.pq('#form_f307 option').length:
            # pas d'accueil de loisirs de vacances, enfant trop jeune ?
            logging.info('passe, pas d’accueil de loisirs')
            return

        # pioche un centre au hasard
        form_data = {'f307': random.choice(response.pq('#form_f307 option')[1:]).attrib['value']}
        self.sleep(5)
        response = response.do_wcs_next_page(params=form_data)

        if response.pq('#steps .current .wcs-step--label-text').text() == 'Dossier enfant':
            # Le Dossier Unique Enfant de ABADIE Eléonore n'est pas à jour.
            # Il est indispensable de mettre à jour ce Dossier Unique Enfant
            # avant de l'inscrire à un accueil de loisirs.
            logging.info('passe, DUE à compléter')
            return

        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Transport'
        self.sleep(3)
        response = response.do_wcs_next_page()

        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Payeur'
        self.sleep(3)
        response = response.do_wcs_next_page()

        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Tarif et facturation'
        self.sleep(3)
        response = response.do_wcs_next_page()

        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Avant validation'
        self.sleep(3)
        response = response.do_wcs_next_page()

        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Validation'
        self.sleep(3)
        response = response.do_wcs_next_page()

        if not response.request.url.endswith('/page-enfant-reservation/'):
            logging.info('erreur après inscription ? (%s)' % response.pq('.global-errors').text())
            return

        # "Réservation et/ou mise à jour calendrier" link is set using javascript
        url = response.pq('.weekly-agenda-cell h3').attr['data-edit-url']
        response = self.client.get(url)
        response.do_wcs_live_request()
        checkbox = [
            PyQuery(x)('input')[0]
            for x in response.pq('.CheckboxesWidget li')
            if PyQuery(x).text() == 'Alsh vacances'
        ][0]
        form_data = {checkbox.attrib['name']: checkbox.attrib['value']}
        response = response.do_wcs_next_page(form_data)
        if response.pq('#steps .current .wcs-step--label-text').text() == 'Dossier enfant':
            # ignore
            response = response.do_wcs_next_page()
        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Réservations'

        form_data = {}
        # select whole week
        # """ Pour les activités d'Alsh vacances, vous devez obligatoirement sélectionner
        # l'ensemble des temps proposés (journées, demi-journées) pour une semaine."""
        # (4 days as christmas and january 1st are off)
        for checkbox in [
            x
            for x in response.pq(
                '.template-famille-reservations-par-semaine--activity-status.free + div input'
            )
        ][:4]:
            form_data[checkbox.attrib['name']] = checkbox.attrib['value']
        self.sleep(5)
        response = response.do_wcs_next_page(form_data)
        if response.pq('#steps .current .wcs-step--label-text').text() == 'Réservations':
            # failed to book
            logging.info('passe, réservation pas possible')
            return

        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Payeur et facturation'
        self.sleep(3)
        response = response.do_wcs_next_page()
        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Tarif(s) et somme à régler'
        self.sleep(3)
        response = response.do_wcs_next_page()
        if response.pq('#steps .current .wcs-step--label-text').text() != 'Avant validation':
            logging.info('passe, %s' % response.pq('.global-errors').text())
            return
        self.sleep(3)
        response = response.do_wcs_next_page()
        assert response.pq('#steps .current .wcs-step--label-text').text() == 'Validation'
        self.sleep(3)
        response = response.do_wcs_next_page()
        assert response.request.url.endswith('/page-enfant-reservation/')
        logging.info('parcours complet (temps passé : %(real)ds / %(total)ds)' % self.get_elapsed_time())
        pass


if __name__ == '__main__':
    DELAYS = False
    run_single_user(PublikFamilleNantesUser)
