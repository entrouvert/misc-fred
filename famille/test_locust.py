# locust -f test_locust.py
# ou juste python test_locust.py pour synchrone / utile pdb

import random
import re
import time
from locust import HttpUser, task, between, run_single_user
import urllib.parse
from requests.models import Response

import locust.clients


def get_name(url):
    parsed = urllib.parse.urlparse(url)
    return f'{parsed.netloc} - {parsed.path}'


class XxxResponse(Response):
    def click(self, selector, sleep=1, **kwargs):
        from pyquery import PyQuery
        pq = PyQuery(self.text)
        if pq.find(selector):
            href = pq.find(selector).attr.href
        else:
            links = [x for x in pq.find('a') if PyQuery(x).text() == selector]
            href = links[0].attrib['href']
        href = urllib.parse.urljoin(self.request.url, href)
        time.sleep(sleep * random.random()*5)
        return self.client.get(href, name=get_name(href), **kwargs)


class HttpAdapter(locust.clients.LocustHttpAdapter):
    def __init__(self, client, **kwargs):
        super().__init__(**kwargs)
        self.client = client

    def build_response(self, req, resp):
        from pyquery import PyQuery
        response = super().build_response(req, resp)
        response.__class__ = XxxResponse
        response.client = self.client
        try:
            pq = PyQuery(response.text)
        except Exception:
            pass
        else:
            # load ajax cells
            for cell_div in pq.find('.cell:not(.card)[data-ajax-cell-url]'):
                if not PyQuery(cell_div).find('[data-ajax-cell-must-load]'):
                    continue
                ajax_url = url = cell_div.attrib['data-ajax-cell-url']
                if not urllib.parse.urlparse(url).scheme:
                    url = urllib.parse.urljoin(response.request.url, url)

                cell_response = self.client.get(url, name=get_name(url))

                def repl(match):
                    return re.sub('><div>.*', f'><div>{cell_response.text}</div></div>', match.group(), flags=re.DOTALL)

                response._content = re.sub(
                        f'<div[^<]*?data-ajax-cell-url="{ajax_url}".*?><div>(.*?)</div></div>',
                        repl,
                        response.text,
                        flags=re.DOTALL).encode()
                pass
        return response


class XxxHttpUser(HttpUser):
    abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client.adapters['https://'] = HttpAdapter(client=self.client, pool_manager=self.client.adapters['https://'].poolmanager)
        self.client.adapters['http://'] = HttpAdapter(client=self.client, pool_manager=self.client.adapters['http://'].poolmanager)


class HelloWorldUser(XxxHttpUser):
    host = 'https://auquo.fred.local.0d.be'
    wait_time = between(1, 3)

    @task
    def hello_world(self):
        response = self.client.get('https://auquo.fred.local.0d.be/backoffice/',
                                   name=get_name('https://auquo.fred.local.0d.be/backoffice/'))
        from pyquery import PyQuery
        login_form = PyQuery(response.text).find('#login-password-form')
        login_data = login_form.serialize_dict()
        login_data['username'] = 'fred'
        login_data['password'] = 'fred'
        login_data['login-password-submit'] = ''
        time.sleep(5)
        response = self.client.post(response.request.url, login_data,
                                    headers={'REFERER': response.request.url},
                                    name='authentic - login')
        assert response.request.url == 'https://auquo.fred.local.0d.be/backoffice/studio/'

        # go to combo
        response = response.click('.ui-platform-name a')
        if 'Se connecter' in response.text:  # autologin didn't work
            response = response.click('Se connecter')
        response = response.click('Mes demandes')
        pass


if __name__ == '__main__':
    run_single_user(HelloWorldUser)
