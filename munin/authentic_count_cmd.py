from django.db import connection
from django.contrib.auth import get_user_model
from authentic2_auth_fc.models import FcAccount

User = get_user_model()

total_user_count = User.objects.filter(userexternalid__isnull=True, ).count()
total_logged_count = User.objects.filter(userexternalid__isnull=True, last_login__isnull=False).count()
fc_count = FcAccount.objects.all().count()

print('total_count.value', total_user_count)
print('total_logged_count.value', total_logged_count)
print('fc_count.value', fc_count)
