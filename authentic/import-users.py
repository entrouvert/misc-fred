import json
import sys

from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType

from django_rbac.utils import get_role_model, get_ou_model
from authentic2.models import Attribute, AttributeValue


User = get_user_model()
Role = get_role_model()
Ou = get_ou_model()

export = json.load(open(sys.argv[1]))

def get_role(role_dict):
    kwargs = {'slug': role_dict['slug']}
    if role_dict['is_service']:
        kwargs['service__slug'] = role_dict['service_slug']
    else:
        kwargs['service__isnull'] = True
    if role_dict['ou__slug']:
        kwargs['ou__slug'] = role_dict['ou__slug']
    else:
        kwargs['ou__isnull'] = True
    return Role.objects.get(**kwargs)

for user_dict in export['users']:
    if user_dict.get('external_id'):
        # don't create external users, hopefully they've already been
        # synchronized.
        try:
            user = User.objects.get(
                    userexternalid__external_id=user_dict['external_id']['id'],
                    userexternalid__source=user_dict['external_id']['source']
            )
        except User.DoesNotExist:
            print 'missing external:', user_dict['external_id']
            continue
    else:
        user, created = User.objects.get_or_create(
            uuid=user_dict['uuid'],
            username=user_dict['username'],
            ou=Ou.objects.get(slug=user_dict['ou__slug']) if user_dict['ou__slug'] else None)
        if created:
            user.password = user_dict['password']
        for attribute in ('email', 'first_name', 'last_name', 'is_superuser', 'email_verified'):
            setattr(user, attribute, user_dict.get(attribute))

        user.save()

    content_type = ContentType.objects.get_for_model(User)
    for attribute_name, attribute_value in user_dict['attributes'].items():
        Attribute.objects.get(name=attribute_name).set_value(user, attribute_value)

    for role_dict in user_dict['roles']:
        try:
            user.roles.add(get_role(role_dict))
        except Role.DoesNotExist:
            print 'failed to add role "%s" to user "%s"' % (role_dict['name'], user)
