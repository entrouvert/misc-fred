import json

from django_rbac.utils import get_role_model, get_ou_model, get_role_parenting_model, get_permission_model
from authentic2.a2_rbac.models import RoleAttribute

Role = get_role_model()
RoleParenting = get_role_parenting_model()
Permission = get_permission_model()
Ou = get_ou_model()

export = {'roles': []}

def to_json(role, attributes=True):
    # to put in Role::to_json() (at least in parts)
    role_dict = role.to_json()
    if role.service_id:
        role_dict['service_slug'] = role.service.slug
    if attributes:
        role_dict['description'] = role.description
        role_dict['attributes'] = {}
        for attribute in RoleAttribute.objects.filter(role=role):
            role_dict['attributes'][attribute.name] = {'kind': attribute.kind, 'value': attribute.value}
    return role_dict

for role in Role.objects.all():
    role_dict = to_json(role)
    role_dict['parents'] = []
    export['roles'].append(role_dict)
    qs = RoleParenting.objects.filter(child_id=role.id, direct=True)
    for parenting in qs:
        role_dict['parents'].append(to_json(parenting.parent, attributes=False))

print json.dumps(export, indent=2)
