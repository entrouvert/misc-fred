# authentic2-multitenant-manage tenant_command runscript resync-roles.py (--all-tenants)

from hobo.agent.authentic2.provisionning import Provisionning
from django_rbac.utils import get_role_model

Role = get_role_model()
engine = Provisionning()
ous = {ou.id: ou for ou in engine.OU.objects.all()}
engine.notify_roles(ous, Role.objects.all(), full=True)
