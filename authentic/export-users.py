import json

from django.contrib.auth import get_user_model
from django_rbac.utils import get_role_model, get_ou_model
from authentic2.models import AttributeValue

User = get_user_model()
Role = get_role_model()
Ou = get_ou_model()

export = {'users': []}

def role_to_json(role, attributes=True):
    # to put in Role::to_json() (at least in parts)
    role_dict = role.to_json()
    if role.service_id:
        role_dict['service_slug'] = role.service.slug
    return role_dict

def to_json(user):
    user_dict = {
        'uuid': user.uuid,
        'username': user.username,
        'email': user.email,
        'ou': user.ou.name if user.ou else None,
        'ou__uuid': user.ou.uuid if user.ou else None,
        'ou__slug': user.ou.slug if user.ou else None,
        'ou__name': user.ou.name if user.ou else None,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'is_superuser': user.is_superuser,
        'password': user.password,
        'email_verified': user.email_verified,
        'roles': [role_to_json(role) for role in user.roles.all()],
        'external_id': {
            'id': user.userexternalid_set.first().external_id,
            'source': user.userexternalid_set.first().source
        } if user.userexternalid_set.count() else None,
        'attributes': {}
    }
    for av in AttributeValue.objects.with_owner(user):
        # XXX: this should also export av.verified
        if av.attribute.kind in ('date', 'birthdate'):
            continue
        user_dict['attributes'][str(av.attribute.name)] = av.to_python()

    return user_dict

for user in User.objects.all():
    user_dict = to_json(user)
    export['users'].append(user_dict)

print json.dumps(export, indent=2)
